function deepClone(obj) {
    if (obj === null || typeof obj !== 'object') {
      return obj; // Базовий випадок: якщо obj є примітивом або null, просто повертаємо його
    }
  
    if (Array.isArray(obj)) {
      const newArray = [];
      for (let i = 0; i < obj.length; i++) {
        newArray[i] = deepClone(obj[i]); // Рекурсивно клонуємо кожен елемент масиву
      }
      return newArray;
    }
  
    const newObj = {};
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        newObj[key] = deepClone(obj[key]); // Рекурсивно клонуємо кожну властивість об'єкта
      }
    }
    return newObj;
  }
  
  // Ще один приклад використання
  const originalObject = {
    a: 1,
    b: [2, 3, 4],
    c: {
      d: 5,
      e: [6, 7],
    },
  };
  
  const clonedObject = deepClone(originalObject);
  console.log(clonedObject);
  